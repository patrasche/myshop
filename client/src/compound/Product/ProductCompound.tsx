import React, { useState, useEffect, useRef, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { ProdData, fetchProducts } from '../../api/product';
import { ProductFilter, ProductList } from '.';
import { RootState } from '../../modules';

const useStyles = makeStyles((theme) =>
  createStyles({
    filterWrap: {
      padding: `${theme.spacing(1)}px 0`,
    },
  })
);

const toNumber = (val: string | number) => {
  return typeof val === 'string'
    ? Number.parseFloat(val.replace(/,/gi, ''))
    : val;
};
const chooseSorter = (sortType: string) => {
  switch (sortType) {
    case 'newest':
      return (a: ProdData, b: ProdData) => {
        return Date.parse(b.created) - Date.parse(a.created);
      };
    case 'lprice':
      return (a: ProdData, b: ProdData) => {
        return toNumber(a.price) - toNumber(b.price);
      };
    case 'eprice':
      return (a: ProdData, b: ProdData) => {
        return toNumber(b.price) - toNumber(a.price);
      };
    default:
      return (a: ProdData, b: ProdData) => {
        return 0;
      };
  }
};

export const ProductCompound = () => {
  const classes = useStyles();
  const { data, error, loading } = useSelector(
    (state: RootState) => state.products.products
  );

  const [prodData, setProdData] = useState<ProdData[]>([]);
  const [resultProdData, setResultProdData] = useState<ProdData[] | null>(null);
  const [search, setSearch] = useState('');
  const [sorting, setSorting] = useState('newest');
  const [viewtype, setViewtype] = useState('big');

  const debounceTimeout = useRef<ReturnType<typeof setTimeout> | null>(null);

  useEffect(() => {
    return () => {
      debounceTimeout.current = null;
    };
  }, []);

  useEffect(() => {
    if (loading) return;
    setProdData(data);
  }, [data, loading]);

  useEffect(() => {
    setProdData((state) => {
      const result = [...state].sort(chooseSorter(sorting));
      return result;
    });
  }, [sorting]);

  useEffect(() => {
    const result = prodData.filter((val) => {
      return val.prodName.toLowerCase().indexOf(search.toLowerCase()) !== -1;
    });
    setResultProdData(result);
  }, [prodData, search]);

  const handleSearch = useCallback((e) => {
    if (e.target.value.length <= 0) {
      setSearch(e.target.value);
      return;
    }
    if (debounceTimeout.current !== null) clearTimeout(debounceTimeout.current);
    debounceTimeout.current = setTimeout(() => {
      return setSearch(e.target.value);
    }, 500);
  }, []);

  const handleSorting = useCallback(
    (e: React.ChangeEvent<{ value: unknown }>) => {
      return setSorting(e.target.value as string);
    },
    []
  );

  const handleViewtype = () => {
    setViewtype((type) => (type === 'col' ? 'big' : 'col'));
  };

  return (
    <>
      <div className={classes.filterWrap}>
        <ProductFilter
          handleSearch={handleSearch}
          viewtype={viewtype}
          handleViewtype={handleViewtype}
          sorting={sorting}
          handleSorting={handleSorting}
        />
      </div>
      <ProductList
        error={error}
        loading={loading}
        viewtype={viewtype}
        productData={resultProdData}
        searching={search.length > 0}
      />
    </>
  );
};
