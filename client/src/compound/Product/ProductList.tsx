import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { ProdData } from '../../api/product';
import { Result } from '../../components/Result';
import { MemoizedProductItem, ProductSkeleton } from '.';

const useStyles = makeStyles(() =>
  createStyles({
    prodList: {
      flexGrow: 1,
    },
  })
);

interface ProductListProps {
  error: any;
  loading: boolean;
  viewtype: string;
  productData: ProdData[] | null;
  searching: boolean;
}

export const ProductList = (props: ProductListProps) => {
  const { error, loading, viewtype, productData, searching } = props;
  const classes = useStyles();
  const viewtypeGridNum = viewtype === 'col' ? 6 : 12;

  if (error)
    return (
      <Result
        status="error"
        title="Fail"
        subTitle="상품 정보 로딩에 실패했습니다"
      />
    );
  if (searching && productData?.length === 0)
    return (
      <Result
        status="info"
        title="검색 결과 없음"
        subTitle="상품명으로만 검색 가능합니다"
      />
    );
  return (
    <div className={classes.prodList}>
      <Grid container spacing={2}>
        {(loading || !productData || productData?.length === 0
          ? Array.from({ length: 4 }, (v, i) => i)
          : productData
        ).map((val: any) => (
          <Grid item key={val.id ? val.id : val} xs={viewtypeGridNum}>
            {val.id ? <MemoizedProductItem prod={val} /> : <ProductSkeleton />}
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
