import React from 'react';
import { IconButton, IconButtonProps } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    btnViewtype: {
      width: '24px',
      height: '24px',
      padding: '0',
      color: '#333',
      '&:before, &:after': {
        display: 'inline-block',
        width: '9px',
        height: '18px',
        margin: '0 1px',
        borderRadius: '2px',
        backgroundColor: '#333',
        content: '""',
      },
    },
    typeCol: {},
    typeBig: {
      '&:before': {
        width: '18px',
      },
      '&:after': {
        display: 'none',
      },
    },
  })
);

interface BtnViewtypeProps extends IconButtonProps {
  viewtypeClass: 'typeCol' | 'typeBig';
  labelText: string;
}

export const BtnViewtype = (props: BtnViewtypeProps) => {
  const { viewtypeClass, labelText, ...rest } = props;
  const classes = useStyles();

  return (
    <IconButton
      {...rest}
      className={`${classes.btnViewtype} ${classes[viewtypeClass]}`}
    >
      <span className="blind">{labelText}</span>
    </IconButton>
  );
};
