import { AxiosError } from 'axios';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../store';
import { ProdData, fetchProducts } from '../api/product';

export interface ProductsState {
  products: {
    data: ProdData[];
    error: Error | null;
    loading: boolean;
  };
}

const initialState: ProductsState = {
  products: {
    data: [],
    error: null,
    loading: false,
  },
};

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    // 함수이름이 키값 ㅎ es6
    getProducts(state) {
      state.products.loading = true;
      state.products.error = null;
    },
    getProductsSuccess(state, action: PayloadAction<ProdData[]>) {
      state.products.data = action.payload;
      state.products.loading = false;
      state.products.error = null;
    },
    getProductsFailure(state, action: PayloadAction<Error>) {
      state.products.loading = false;
      state.products.error = action.payload;
    },
  },
});

export const {
  getProducts,
  getProductsSuccess,
  getProductsFailure,
} = productsSlice.actions;

// dispatch는 리덕스 함수
// dispatch 에 타입 에러뜬거는 AppThunk 라는 타입을 명시해주니 사라짐
export const getProductsThunk = (count: number = 12): AppThunk => async (
  dispatch
) => {
  try {
    dispatch(getProducts());
    const products = await fetchProducts(count);
    dispatch(getProductsSuccess(products));
  } catch (e) {
    dispatch(getProductsFailure(e));
  }
};

export default productsSlice.reducer;
