import React from 'react';
import { Layout } from '../../components/Layouts';

const PageName = () => {
  return (
    <Layout>
      {/* content > inner (s) */}
      PageName
      {/* content > inner (e) */}
    </Layout>
  );
};

export default PageName;
