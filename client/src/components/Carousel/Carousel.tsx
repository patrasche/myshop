import React, { useState, useRef } from 'react';
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import PlayCircleFilledRoundedIcon from '@material-ui/icons/PlayCircleFilledRounded';
import PauseCircleFilledRoundedIcon from '@material-ui/icons/PauseCircleFilledRounded';

import 'swiper/swiper-bundle.css';
import './swiperCustom.css';

// 네비게이션 버튼
interface BtnNavigationProps {
  className: string;
  a11yText: string;
}
const BtnNavigation = ({ className, a11yText }: BtnNavigationProps) => {
  return (
    <a
      href="none"
      role="button"
      className={className}
      onClick={(e) => e.preventDefault()}
    >
      <span className="blind">{a11yText}</span>
    </a>
  );
};

// 자동재생 toggle 버튼
interface BtnPlayProps {
  onClick: (e: any) => void;
  isPlay: boolean;
}
const BtnPlay = ({ onClick, isPlay }: BtnPlayProps) => {
  return (
    <a href="none" role="button" className="swiper-play" onClick={onClick}>
      {isPlay ? (
        <>
          <PauseCircleFilledRoundedIcon />
          <span className="blind">자동재생 끄기</span>
        </>
      ) : (
        <>
          <PlayCircleFilledRoundedIcon />
          <span className="blind">자동재생 켜기</span>
        </>
      )}
    </a>
  );
};

// swiper
SwiperCore.use([Navigation, Pagination, Autoplay]);
export const Carousel = ({ children }: SwiperSlide) => {
  const [play, setPlay] = useState(true);

  const oSwiper = useRef<SwiperCore | null>(null);

  const playCarousel = () => {
    oSwiper.current?.autoplay.start();
    setPlay(true);
  };
  const stopCarousel = () => {
    oSwiper.current?.autoplay.stop();
    setPlay(false);
  };

  const handlePlayClick = (e: any) => {
    e.preventDefault();
    if (play) {
      stopCarousel();
    } else {
      playCarousel();
    }
  };

  const handleSlideChange = (swiper: SwiperCore) => {
    swiper.slides.forEach((val, idx) => {
      if (idx !== swiper.activeIndex) {
        val.setAttribute('aria-hidden', 'true');
      } else {
        val.removeAttribute('aria-hidden');
      }
    });
  };

  const navigationOpts = {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  };

  const paginationOpts: any = {
    el: '.swiper-pagination',
    type: 'fraction',
    renderFraction: (current: string, total: string) => {
      return `<span class="blind">현재 배너 번호</span><span class=${current}></span><span aria-hidden="true" class="separator">/</span><span class="blind">전체 배너 개수</span><span class=${total}></span>`;
    },
  };

  return (
    <Swiper
      loop
      navigation={navigationOpts}
      autoplay
      pagination={paginationOpts}
      onSwiper={(swiper) => {
        oSwiper.current = swiper;
      }}
      onSlideChange={handleSlideChange}
      onAutoplayStart={() => setPlay(true)}
      onAutoplayStop={() => setPlay(false)}
    >
      {children}
      <span slot="container-start">
        <div className="swiper-pagination-wrap">
          <span className="swiper-pagination" />
          <BtnPlay isPlay={play} onClick={handlePlayClick} />
        </div>
        <BtnNavigation
          className="swiper-button-next"
          a11yText="다음 배너 보기"
        />
        <BtnNavigation
          className="swiper-button-prev"
          a11yText="이전 배너 보기"
        />
      </span>
    </Swiper>
  );
};
