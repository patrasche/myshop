import React from 'react';
import { Story } from '@storybook/react';

import { Result, ResultProps } from './Result';

export default {
  title: 'Data Display/Result',
  component: Result,
};

const Template: Story<ResultProps> = (args) => <Result {...args} />;

export const Default = Template.bind({});
Default.args = {
  status: 'info',
  title: 'Information',
  subTitle: 'Lorem ipsum dolor sit amet consectetur adipisicing.',
  extra: <button type="button">button</button>,
};

export const SuccessResult = Template.bind({});
SuccessResult.args = {
  status: 'success',
  title: 'Success',
};

export const WarningResult = Template.bind({});
WarningResult.args = {
  status: 'warning',
  title: 'Warning',
};

export const ErrorResult = Template.bind({});
ErrorResult.args = {
  status: 'error',
  title: 'Error',
};

export const ResultWithoutIcon = Template.bind({});
ResultWithoutIcon.args = {
  title: 'Hello',
  subTitle: 'Nice to meet you!',
};
