import { ThemeOptions } from '@material-ui/core';

export const inputTheme: ThemeOptions = {
  props: {
    // 컴포넌트 props
    MuiButtonBase: {
      disableRipple: true,
    },
    MuiInput: {
      disableUnderline: true,
    },
  },
  overrides: {
    MuiIconButton: {
      root: {
        '&:hover': {
          backgroundColor: 'transparent',
        },
      },
      label: {
        width: '0',
      },
    },
    MuiInputBase: {
      input: {
        fontSize: '0.9rem',
      },
    },
    MuiSelect: {
      select: {
        '&:focus': {
          backgroundColor: 'transparent',
        },
      },
      icon: {
        color: '#333',
      },
    },
  },
};
