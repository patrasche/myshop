# E-Commerce 페이지 제작

## 기술스택

- React
- Javascript(ES6)
- Typescript
- Material-UI
- Storybook

## 배포

**E-Commerce(My Shop):**<br>
https://patrasche-shop.herokuapp.com/<br>
(배포 서비스 heroku는 접속하지 않으면 서버가 잠들어있어서 초기 접속시 Idle time이 조금 오래걸릴 수 있습니다.)

**Storybook:** <br>
https://patrasche.gitlab.io/myshop/client/storybook-static/

## 화면/브라우저 대응

- 화면: 모바일용 페이지
- OS: Android 4.4 이상 지원
- 브라우저: OperaMini 브라우저 제외 최신 브라우저 대응


## 렌더링 최적화

![image](/uploads/bab740e2263ffc7d52d39f7860b250ef/image.png)
> React 개발자도구 설치 후 `Highlight updates when components render.` 옵션을 활성화 하면 렌더링이 발생할 때 마다 해당 렌더링되는 요소를 확인 할 수 있다.

### Search onChange : debounce 적용

| 적용 전 | 적용 후 (일단 500ms로 적용) |
| ------ | ------ |
| ![search_before](/uploads/d1e41c49f4e35d7379f82df83cd4bd78/search_before.gif) | ![search_after](/uploads/5818a6d41138ed3cfca8fd8eba516bb4/search_after.gif) |

* 한글의 경우 자음모음 구성으로 문자가 완성되기 때문에 문자를 입력하는 매순간 업데이트하면 결과 없음이 자주 발생
* 영문의 경우도 하나의 단어를 구성하는 최소한의 몇글자를 입력하는 동안에 너무 잦은 업데이트가 있을 수 있음
* 반응이 느리다는 느낌은 주지 않을 정도의 타이밍으로 debounce 적용이 필요함<br>
  > 적절한 타이밍에 대한 의견: https://ux.stackexchange.com/questions/95336/how-long-should-the-debounce-timeout-be<br>
  > 500ms 정도를 일단 테스트 타이밍으로 잡아볼만 하다. 사용자의 타이핑 속도, 특히 모바일의 경우 PC보다 느린 타이핑 속도일 수 있음을 고려하여 사용자의 피드백을 받아 적절한 타이밍을 잡는게 좋다.

### ProductItem : React.memo 적용

| 적용 전 | 적용 후 |
| ------ | ------ |
| ![memo_before](/uploads/214832b070135f48176b6e4a7f3329d1/memo_before.gif) | ![memo_after](/uploads/3e166960c9ec07ab6cedb457c5bfdf12/memo_after.gif) |

https://ui.toast.com/weekly-pick/ko_20190731
> props가 변경 되지 않는다면 다음 렌더링 때 메모이징 된 내용을 그대로 사용하게 된다.
