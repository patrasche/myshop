import React from 'react';
import { Story } from '@storybook/react';

import { SkeletonText, SkeletonTextProps } from './SkeletonText';

export default {
  title: 'Data Display/Skeleton/Text',
  component: SkeletonText,
};

const Template: Story<SkeletonTextProps> = (args) => (
  <div style={{ width: '50%', height: '300px' }}>
    <SkeletonText {...args} />
    <SkeletonText {...args} />
    <SkeletonText {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {};

export const SquaredSkeletonText = Template.bind({});
SquaredSkeletonText.args = {
  rounded: false,
};

export const OffAniSkeletonText = Template.bind({});
OffAniSkeletonText.args = {
  animation: false,
};

export const FixedSizeSkeletonText = Template.bind({});
FixedSizeSkeletonText.args = {
  usePaddingTop: false,
  width: '150px',
  height: '20px',
};
