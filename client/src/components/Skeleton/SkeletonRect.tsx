import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  skeleton: {
    display: 'block',
    paddingTop: '57%',
    backgroundColor: 'rgba(0,0,0,0.11)',
  },
  ani: {
    animation: 'blink 1.5s ease-in-out 0.5s infinite',
  },
  rounded: {
    borderRadius: '3px',
  },
});

export interface SkeletonRectProps {
  animation?: boolean;
  rounded?: boolean;
  usePaddingTop?: boolean;
  width?: string | undefined;
  height?: string | undefined;
}

const SkeletonRect = (props: SkeletonRectProps) => {
  const { animation, rounded, usePaddingTop, width, height } = props;
  const classes = useStyles();

  return (
    <span
      className={`${classes.skeleton} ${animation ? classes.ani : ''} ${
        rounded ? classes.rounded : ''
      }`}
      style={{
        width: width && width,
        height: !usePaddingTop && height ? height : '',
        paddingTop: usePaddingTop ? height && height : 0,
      }}
    />
  );
};

SkeletonRect.defaultProps = {
  animation: true,
  rounded: true,
  usePaddingTop: true,
  width: undefined,
  height: undefined,
};

export { SkeletonRect };
