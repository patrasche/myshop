import React from 'react';
import { Story } from '@storybook/react';

import { SkeletonRect, SkeletonRectProps } from './SkeletonRect';

export default {
  title: 'Data Display/Skeleton/Rect',
  component: SkeletonRect,
};

const Template: Story<SkeletonRectProps> = (args) => (
  <div style={{ width: '50%', height: '300px' }}>
    <SkeletonRect {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {};

export const SquaredSkeletonRect = Template.bind({});
SquaredSkeletonRect.args = {
  rounded: false,
};

export const OffAniSkeletonRect = Template.bind({});
OffAniSkeletonRect.args = {
  animation: false,
};

export const FixedSizeSkeletonRect = Template.bind({});
FixedSizeSkeletonRect.args = {
  usePaddingTop: false,
  width: '200px',
  height: '100px',
};
