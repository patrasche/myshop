import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  skeleton: {
    display: 'block',
    padding: '8px 0',
    '& + &': {
      paddingTop: 0,
    },
  },
  cont: {
    display: 'block',
    height: '12px',
    backgroundColor: 'rgba(0,0,0,0.11)',
  },
  ani: {
    animation: 'blink 1.5s ease-in-out 0.5s infinite',
  },
  rounded: {
    borderRadius: '3px',
  },
});

export interface SkeletonTextProps {
  animation?: boolean;
  rounded?: boolean;
  usePaddingTop?: boolean;
  width?: string | undefined;
  height?: string | undefined;
}

const SkeletonText = (props: SkeletonTextProps) => {
  const { animation, rounded, usePaddingTop, width, height } = props;
  const classes = useStyles();

  return (
    <span className={`${classes.skeleton} ${animation ? classes.ani : ''}`}>
      <span
        className={`${classes.cont} ${rounded ? classes.rounded : ''}`}
        style={{
          width: width && width,
          height: usePaddingTop ? 0 : height && height,
          paddingTop: usePaddingTop && height ? height : '',
        }}
      />
    </span>
  );
};

SkeletonText.defaultProps = {
  animation: true,
  rounded: true,
  usePaddingTop: false,
  width: undefined,
  height: undefined,
};

export { SkeletonText };
