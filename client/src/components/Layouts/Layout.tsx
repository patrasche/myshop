import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { LayoutInner, Header, Content, Footer } from '.';

const useStyles = makeStyles(() =>
  createStyles({
    wrap: {},
  })
);

interface LayoutProps extends React.HTMLProps<HTMLElement> {}

const Layout = ({ children }: LayoutProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrap}>
      <Header />
      <Content>
        <LayoutInner>{children}</LayoutInner>
      </Content>
      <Footer />
    </div>
  );
};

export default Layout;
