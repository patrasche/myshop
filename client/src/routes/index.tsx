import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Home from '../pages/home';
import Details from '../pages/details';

const useStyles = makeStyles({
  notFound: {
    display: 'flex',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

const Pages = () => {
  const classes = useStyles();

  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/details/:prodId?" component={Details} />
      <Route
        render={({ location }) => (
          <div className={classes.notFound}>
            <div>
              <h2>이 페이지는 아직 준비중입니다 :</h2>
              <p>{location.pathname}</p>
            </div>
          </div>
        )}
      />
    </Switch>
  );
};

export default Pages;
