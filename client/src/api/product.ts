import { api } from './instance';

export interface ProdData {
  id: string;
  brandName: string;
  prodName: string;
  colors: string[];
  price: number | string;
  imgSrc: string;
  outOfStock: boolean;
  created: string; // api 를 보낼때는 created가 Data 타입이었지만 받고보니 string 타입으로 넘어와서 여긴 string 으로 처리.
}

export const fetchProducts = async (payload: number = 18) => {
  const response = await api.post<ProdData[]>('/api/product/main', {
    count: payload,
  });
  return response.data;
};
