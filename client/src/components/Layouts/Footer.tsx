import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { LayoutInner } from '.';

const useStyles = makeStyles((theme) =>
  createStyles({
    footer: {
      background: '#ededed',
    },
    cont: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100px',
      padding: `0 ${theme.spacing(2)}px`,
      textAlign: 'center',
    },
    copy: {
      color: '#000',
    },
  })
);

const Footer = () => {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <LayoutInner>
        <div className={classes.cont}>
          <p className={classes.copy}>2021, My shop.</p>
        </div>
      </LayoutInner>
    </footer>
  );
};

export default Footer;
