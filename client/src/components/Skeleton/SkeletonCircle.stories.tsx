import React from 'react';
import { Story } from '@storybook/react';

import { SkeletonCircle, SkeletonCircleProps } from './SkeletonCircle';

export default {
  title: 'Data Display/Skeleton/Circle',
  component: SkeletonCircle,
};

const Template: Story<SkeletonCircleProps> = (args) => (
  <div style={{ width: '50%', height: '300px' }}>
    <SkeletonCircle {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {};

export const OffAniSkeletonCircle = Template.bind({});
OffAniSkeletonCircle.args = {
  animation: false,
};

export const FixedSizeSkeletonCircle = Template.bind({});
FixedSizeSkeletonCircle.args = {
  usePaddingTop: false,
  width: '200px',
  height: '100px',
};
