import React from 'react';
import { Story } from '@storybook/react';

import { InputText, InputTextProps } from './InputText';

export default {
  title: 'Form/InputText',
  component: InputText,
};

const Template: Story<InputTextProps> = (args) => (
  <div style={{ display: 'inline-block', border: '1px solid #888' }}>
    <InputText {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {
  placeholder: '검색어를 입력하세요',
  onChange: (e) => {
    console.log(e.target.value);
  },
};
