import React, { useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { SwiperSlide } from 'swiper/react';
import { getProductsThunk } from '../../modules/products';
import { BannerData } from '../../api/banner';

import { Layout } from '../../components/Layouts';
import { Carousel } from '../../components/Carousel';
import { Banner } from '../../components/Banner';
import { ProductCompound } from '../../compound/Product';

const useStyles = makeStyles((theme) =>
  createStyles({
    carousel: {
      borderBottom: '1px solid #ccc',
    },
    titleWrap: {
      marginTop: theme.spacing(4),
      textAlign: 'center',
    },
    title: {
      fontSize: '22px',
      color: '#333',
    },
    prodWrap: {
      padding: `0 ${theme.spacing(2)}px ${theme.spacing(4)}px`,
    },
  })
);

const bannerData: BannerData[] = [
  {
    id: 1,
    imgSrc: './assets/banner/1.jpg',
    flag: 'HOT',
    mainCopy: 'Lorem, ipsum.',
    subCopy: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.',
  },
  {
    id: 2,
    imgSrc: './assets/banner/2.jpg',
    flag: 'NEW',
    mainCopy: 'Lorem, ipsum.',
    subCopy:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident.',
  },
  {
    id: 3,
    imgSrc: './assets/banner/3.jpg',
    flag: 'EVENT',
    mainCopy: 'Lorem, ipsum.',
    subCopy:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis, dicta.',
  },
  {
    id: 4,
    imgSrc: './assets/banner/4.jpg',
    flag: 'BEST',
    mainCopy: 'Lorem, ipsum.',
    subCopy:
      'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quibusdam, dolor explicabo?',
  },
];

const Home = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsThunk());
  }, [dispatch]);

  return (
    <Layout>
      {/* content > inner (s) */}
      <div className={classes.carousel}>
        <Carousel>
          {bannerData.map((val) => {
            return (
              <SwiperSlide key={val.id}>
                <Link to={`/event/${val.id}`}>
                  <Banner
                    imgSrc={val.imgSrc}
                    flag={val.flag}
                    mainCopy={val.mainCopy}
                    subCopy={val.subCopy}
                  />
                </Link>
              </SwiperSlide>
            );
          })}
        </Carousel>
      </div>
      <div className={classes.titleWrap}>
        <h2 className={classes.title}>Best Items</h2>
      </div>
      <div className={classes.prodWrap}>
        <ProductCompound />
      </div>
      {/* content > inner (e) */}
    </Layout>
  );
};

export default Home;
