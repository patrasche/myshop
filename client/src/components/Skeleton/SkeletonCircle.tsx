import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  skeleton: {
    display: 'block',
    paddingTop: '100%',
    borderRadius: '50%',
    backgroundColor: 'rgba(0,0,0,0.11)',
  },
  ani: {
    animation: 'blink 1.5s ease-in-out 0.5s infinite',
  },
});

export interface SkeletonCircleProps {
  animation?: boolean;
  usePaddingTop?: boolean;
  width?: string | undefined;
  height?: string | undefined;
}

const SkeletonCircle = (props: SkeletonCircleProps) => {
  const { animation, usePaddingTop, width, height } = props;
  const classes = useStyles();

  return (
    <span
      className={`${classes.skeleton} ${animation ? classes.ani : ''}`}
      style={{
        width: width && width,
        height: !usePaddingTop && height ? height : '',
        paddingTop: usePaddingTop ? height && height : 0,
      }}
    />
  );
};

SkeletonCircle.defaultProps = {
  animation: true,
  usePaddingTop: true,
  width: undefined,
  height: undefined,
};

export { SkeletonCircle };
