import React from 'react';
import { MenuItem, FormControl, Select } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import SwapVertRoundedIcon from '@material-ui/icons/SwapVertRounded';
import { InputText } from '../../components/Inputs';
import { BtnViewtype } from '.';

const useStyles = makeStyles((theme) =>
  createStyles({
    filter: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    search: {
      flex: 1,
      marginRight: theme.spacing(1),
    },
    form: {
      display: 'flex',
      alignItems: 'center',
      padding: `${theme.spacing(0.5)}px 0`,
    },
    searchIcon: {
      marginRight: theme.spacing(0.5),
      color: '#333',
    },
    input: {
      flex: 1,
    },
    utils: {
      flex: 1,
      textAlign: 'right',
    },
    formControl: {
      marginRight: theme.spacing(1),
      color: '#333',
      verticalAlign: 'middle',
    },
  })
);

interface FilterProps {
  handleSearch: any;
  viewtype: string;
  handleViewtype: any;
  sorting: string;
  handleSorting: any;
}

export const ProductFilter = (props: FilterProps) => {
  const {
    handleSearch,
    viewtype,
    handleViewtype,
    sorting,
    handleSorting,
  } = props;
  const classes = useStyles();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) =>
    e.preventDefault();

  return (
    <div className={classes.filter}>
      <div className={classes.search}>
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <SearchIcon className={classes.searchIcon} />
          <InputText
            className={classes.input}
            placeholder="상품명을 검색하세요"
            inputProps={{ 'aria-label': '상품검색' }}
            onChange={handleSearch}
          />
        </form>
      </div>
      <div className={classes.utils}>
        <FormControl className={classes.formControl}>
          <Select
            value={sorting}
            onChange={handleSorting}
            IconComponent={SwapVertRoundedIcon}
            inputProps={{ title: '상품목록 정렬방식' }}
          >
            <MenuItem value="newest">최근등록순</MenuItem>
            <MenuItem value="lprice">가격낮은순</MenuItem>
            <MenuItem value="eprice">가격높은순</MenuItem>
          </Select>
        </FormControl>
        <BtnViewtype
          viewtypeClass={viewtype === 'col' ? 'typeCol' : 'typeBig'}
          labelText={
            viewtype === 'col' ? '상품목록 크게보기' : '상품목록 2단으로 보기'
          }
          onClick={handleViewtype}
        />
      </div>
    </div>
  );
};
