export interface BannerData {
  id: number;
  imgSrc: string;
  flag: string;
  mainCopy: string;
  subCopy: string;
}
