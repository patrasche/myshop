import { ThemeOptions } from '@material-ui/core';

export const svgIconTheme: ThemeOptions = {
  overrides: {
    MuiSvgIcon: {
      root: {
        verticalAlign: 'middle',
      },
    },
  },
};
