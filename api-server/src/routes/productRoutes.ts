import express from 'express';
import faker from 'faker';

const product = express.Router();

product.post('/main', (req, res) => {
  const { count = 18 } = req.body;
  console.log('Receive Main product info');

  const products = createProduct(count);
  products.sort((a, b) => {
    return +b.created - +a.created; // created 는 랜덤순서로 부여되기 때문에 처음부터 정렬해놓은 데이터를 보내주자 (여기에서 받아온건 Data 타입이라 +표기 활용으로 Data를 정렬함)
  })
  products[0] && (products[0].imgSrc = '/assets/prod/1.jpg');
  products[1] && (products[1].imgSrc = '/assets/prod/2.jpg');
  products[2] && (products[2].imgSrc = '/assets/prod/3.jpg');
  products[3] && (products[3].imgSrc = '/assets/prod/4.jpg');
  products[4] && (products[4].imgSrc = '/assets/prod/5.jpg');
  products[5] && (products[5].imgSrc = '/assets/prod/6.jpg');

  res.send(products);
});

interface ProdData {
  id: string;
  brandName: string;
  prodName: string;
  colors: string[];
  price: number | string;
  imgSrc: string;
  outOfStock: boolean;
  created: Date;
}

export default product;

const createProduct = (number: number) => {
  return Array.from({ length: number }).reduce<ProdData[]>((acc) => {
    acc.push({
      id: faker.datatype.uuid(),
      brandName: faker.vehicle.manufacturer(),
      prodName: `${faker.vehicle.model()} ${faker.random.words(2)}`,
      colors: getColors(),
      price: (faker.datatype.number(170) * 10000 + 300000).toLocaleString('ko'),
      imgSrc: 'https://via.placeholder.com/750x490',
      outOfStock: faker.datatype.boolean(),
      created: faker.datatype.datetime(Date.now()),
    });

    return acc;
  }, []);
};

function getColors() {
  let colors: string[] = [];
  const colorData = Array.from({ length: Math.floor(Math.random() * 4) + 1 }).map(() => {
    let color = faker.commerce.color();
    while (color.indexOf(' ') >= 0 || color === 'magenta' || colors.indexOf(color) >= 0) {
      color = faker.commerce.color();
    }
    colors.push(color);
    return color;
  });
  return colorData;
}
