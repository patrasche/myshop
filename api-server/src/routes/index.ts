import express from 'express';
import productRoutes from './productRoutes';

const router = express.Router();

router.use('/api/product', productRoutes);

export default router;
