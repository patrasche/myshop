import React from 'react';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Layout } from '../../components/Layouts';

const useStyles = makeStyles({
  detailWrap: {
    display: 'flex',
    height: 'calc(100vh - 160px)',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

const Details = () => {
  const { prodId } = useParams<{ prodId: string }>();
  const classes = useStyles();

  return (
    <Layout>
      {/* content > inner (s) */}
      <div className={classes.detailWrap}>
        <div>
          <h2>detail 페이지</h2>
          <p>상품ID: {prodId}</p>
        </div>
      </div>
      {/* content > inner (e) */}
    </Layout>
  );
};

export default Details;
