import React from 'react';
import { InputBase, InputBaseProps } from '@material-ui/core';

export interface InputTextProps extends InputBaseProps {}

export const InputText = (props: InputTextProps) => {
  return <InputBase {...props} />;
};
