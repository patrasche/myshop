import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import InfoRoundedIcon from '@material-ui/icons/InfoRounded';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';

const useStyles = makeStyles((theme) =>
  createStyles({
    result: {
      padding: `${theme.spacing(3)}px ${theme.spacing(1)}px`,
    },
    icon: {
      marginBottom: theme.spacing(2),
      textAlign: 'center',
    },
    svgIcon: {
      fontSize: '48px',
      color: '#888',
    },
    title: {
      color: '#333',
      fontSize: '24px',
      lineHeight: 1.8,
      textAlign: 'center',
    },
    subTitle: {
      color: '#888',
      fontSize: '14px',
      lineHeight: 1.6,
      textAlign: 'center',
    },
    extra: {
      marginTop: theme.spacing(2),
      textAlign: 'center',
    },
  })
);

export interface ResultProps {
  status?: 'info' | 'success' | 'warning' | 'error' | undefined;
  title?: string | undefined;
  subTitle?: string | undefined;
  extra?: React.ReactNode | undefined;
}

export const Result = (props: ResultProps) => {
  const classes = useStyles();
  const { status, title, subTitle, extra } = props;
  let icon;
  switch (status) {
    case 'info':
      icon = <InfoRoundedIcon className={classes.svgIcon} />;
      break;
    case 'success':
      icon = <CheckCircleRoundedIcon className={classes.svgIcon} />;
      break;
    case 'warning':
      icon = <WarningRoundedIcon className={classes.svgIcon} />;
      break;
    case 'error':
      icon = <CancelRoundedIcon className={classes.svgIcon} />;
      break;
    default:
      break;
  }

  return (
    <div className={classes.result}>
      {status && <div className={classes.icon}>{icon}</div>}
      {title && <div className={classes.title}>{title}</div>}
      {subTitle && <div className={classes.subTitle}>{subTitle}</div>}
      {extra && <div className={classes.extra}>{extra}</div>}
    </div>
  );
};

Result.defaultProps = {
  status: undefined,
  title: undefined,
  subTitle: undefined,
  extra: undefined,
};
