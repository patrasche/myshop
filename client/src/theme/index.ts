import { createMuiTheme, colors } from '@material-ui/core';
import { svgIconTheme } from './SvgIcon';
import { inputTheme } from './Inputs';

export const theme = createMuiTheme(
  {
    spacing: 8,
    shape: {
      borderRadius: 2,
    },
    palette: {},
    props: {}, // 컴포넌트 props
    overrides: {
      // CSS
      MuiCssBaseline: {
        '@global': {
          html: {
            height: '100%',
            WebkitTextSizeAdjust: '100%',
          },
          body: {
            minHeight: '100%',
            backgroundColor: 'transparent',
            lineHeight: 'normal',
            fontFamily: 'arial, sans-serif',
            WebkitFontSmoothing: 'antialiased',
            MozOsxFontSmoothing: 'grayscale',
          },
          'body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, legend, textarea, p, blockquote, th, td, input, select, textarea, button': {
            margin: 0,
            padding: 0,
          },
          'dl, ul, ol, li': {
            listStyle: 'none',
          },
          a: {
            textDecoration: 'none',
            WebkitBoxSizing: 'border-box',
            boxSizing: 'border-box',
          },
          img: {
            border: '0 none',
            verticalAlign: 'top',
          },
          '.blind': {
            display: 'block',
            overflow: 'hidden',
            position: 'absolute',
            width: '1px',
            height: '1px',
            fontSize: '1px',
            lineHeight: '1px',
            clip: 'rect(1px, 1px, 1px, 1px)',
          },
          '@keyframes blink': {
            '0%': {
              opacity: 1,
            },
            '50%': {
              opacity: 0.4,
            },
            '100%': {
              opacity: 1,
            },
          },
        },
      },
    },
  },
  svgIconTheme,
  inputTheme
);
