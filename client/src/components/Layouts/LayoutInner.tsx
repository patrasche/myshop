import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    inner: {
      maxWidth: '750px',
      margin: '0 auto',
    },
  })
);

interface InnerProps extends React.HTMLProps<HTMLElement> {}

const LayoutInner = ({ children }: InnerProps) => {
  const classes = useStyles();

  return <div className={classes.inner}>{children}</div>;
};

export default LayoutInner;
