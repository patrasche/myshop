export { SkeletonRect } from './SkeletonRect';
export { SkeletonCircle } from './SkeletonCircle';
export { SkeletonText } from './SkeletonText';
