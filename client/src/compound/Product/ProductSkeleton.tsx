import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { SkeletonRect, SkeletonText } from '../../components/Skeleton';

const useStyles = makeStyles((theme) =>
  createStyles({
    detail: {
      padding: `${theme.spacing(1)}px 0`,
    },
  })
);

export const ProductSkeleton = () => {
  const classes = useStyles();

  return (
    <div>
      <SkeletonRect height="65.33%" />
      <div className={classes.detail}>
        <SkeletonText height="17px" />
        <SkeletonText height="17px" />
        <SkeletonText height="17px" />
      </div>
    </div>
  );
};
