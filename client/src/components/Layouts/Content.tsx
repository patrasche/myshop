import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    content: {
      minHeight: 'calc(100vh - 160px)',
    },
  })
);

interface ContentProps extends React.HTMLProps<HTMLElement> {}

const Content = ({ children }: ContentProps) => {
  const classes = useStyles();

  return <div className={classes.content}>{children}</div>;
};

export default Content;
