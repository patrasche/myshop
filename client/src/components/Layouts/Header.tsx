import React from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import PersonOutlineRoundedIcon from '@material-ui/icons/PersonOutlineRounded';
import AddShoppingCartRoundedIcon from '@material-ui/icons/AddShoppingCartRounded';
import { LayoutInner } from '.';

const useStyles = makeStyles((theme) =>
  createStyles({
    header: {
      background: '#fff',
    },
    cont: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: '60px',
      padding: `0 ${theme.spacing(2)}px`,
    },
    logo: {
      color: '#000',
    },
    btn: {
      float: 'left',
      margin: '0 5px',
      color: '#000',
    },
  })
);

const Header = () => {
  const classes = useStyles();

  return (
    <header className={classes.header}>
      <LayoutInner>
        <div className={classes.cont}>
          <h1>
            <Link to="/" className={classes.logo}>
              My Shop
            </Link>
          </h1>
          <div>
            <Link to="/MyPage" className={classes.btn}>
              <PersonOutlineRoundedIcon />
              <span className="blind">계정관리</span>
            </Link>
            <Link to="/Cart" className={classes.btn}>
              <AddShoppingCartRoundedIcon />
              <span className="blind">장바구니</span>
            </Link>
          </div>
        </div>
      </LayoutInner>
    </header>
  );
};

export default Header;
