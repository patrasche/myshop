import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  banner: {
    position: 'relative',
    color: '#333',
  },
  link: {
    display: 'block',
  },
  img: {
    width: '100%',
  },
  copybox: {
    padding: '20px 15px 30px',
    lineHeight: 1.5,
    textAlign: 'center',
  },
  flag: {
    marginBottom: '6px',
    fontSize: '12px',
    fontWeight: 'bold',
  },
  mainCopy: {
    marginBottom: '6px',
    fontSize: '20px',
    fontWeight: 'bold',
  },
  subCopy: {
    width: '300px',
    margin: '0 auto',
    fontSize: '14px',
  },
});

interface BannerProps {
  imgSrc: string;
  flag: string;
  mainCopy: string;
  subCopy: string;
}

export const Banner = (props: BannerProps) => {
  const { imgSrc, flag, mainCopy, subCopy } = props;
  const classes = useStyles();

  return (
    <div className={classes.banner}>
      <img src={imgSrc} alt="" className={classes.img} />
      <div className={classes.copybox}>
        {flag && <p className={classes.flag}>{flag}</p>}
        {mainCopy && <p className={classes.mainCopy}>{mainCopy}</p>}
        {subCopy && <p className={classes.subCopy}>{subCopy}</p>}
      </div>
    </div>
  );
};
