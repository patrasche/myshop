import React from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { ProdData } from '../../api/product';

const useStyles = makeStyles((theme) =>
  createStyles({
    prodItem: {
      color: '#222',
      textAlign: 'left',
    },
    link: {
      display: 'block',
      color: 'inherit',
    },
    thmb: {
      overflow: 'hidden',
      position: 'relative',
      paddingTop: '65.33%',
      borderRadius: '8px',
      '&:after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, 0.04)',
        content: '""',
      },
    },
    thmbImg: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
    },
    detail: {
      padding: `${theme.spacing(1)}px 0`,
    },
    name: {
      display: '-webkit-box',
      overflow: 'hidden',
      WebkitLineClamp: 3,
      WebkitBoxOrient: 'vertical',
      marginTop: theme.spacing(1),
      fontSize: '14px',
      lineHeight: '17px',
      wordWrap: 'break-word',
      wordBreak: 'break-all',
      '&:fisrt-child': {
        marginTop: 0,
      },
    },
    brand: {
      fontStyle: 'normal',
      fontWeight: 'bold',
    },
    colors: {
      marginTop: theme.spacing(1),
      padding: '0 1px',
      '& ul': {
        display: 'flex',
      },
      '& li': {
        marginRight: theme.spacing(0.5),
      },
    },
    colorChip: {
      display: 'block',
      position: 'relative',
      width: '14px',
      height: '14px',
      borderRadius: '2px',
      '&:before': {
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        borderRadius: '2px',
        border: '1px solid rgba(0,0,0,0.15)',
        content: '""',
      },
    },
    priceBox: {
      marginTop: theme.spacing(1),
    },
    price: {
      fontSize: '16px',
      lineHeight: '20px',
      '.MuiGrid-grid-xs-12 &': {
        display: 'inline-block',
      },
    },
    priceNum: {
      fontWeight: 'bold',
    },
    priceTxt: {
      fontSize: '14px',
      lineHeight: '18px',
    },
    outOfStock: {
      '.MuiGrid-grid-xs-12 &': {
        display: 'inline-block',
        marginLeft: theme.spacing(1),
      },
      '.MuiGrid-grid-xs-6 &': {
        marginTop: theme.spacing(0.5),
      },
    },
    outOfStockTxt: {
      display: 'inline-block',
      padding: `${theme.spacing(0.25)}px ${theme.spacing(0.5)}px`,
      borderRadius: '2px',
      backgroundColor: theme.palette.error.dark,
      fontSize: '12px',
      color: '#fff',
    },
  })
);

interface ProductItemProps {
  prod: ProdData;
}

export const ProductItem = (props: ProductItemProps) => {
  const { prod } = props;
  const classes = useStyles();

  return (
    <div className={classes.prodItem}>
      <Link to={`/details/${prod.id}`} className={classes.link}>
        <div className={classes.thmb}>
          <img
            className={classes.thmbImg}
            src={prod.imgSrc}
            alt={prod.prodName}
          />
        </div>
        <div className={classes.detail}>
          <div className={classes.name}>
            <em className={classes.brand}>{prod.brandName}</em>{' '}
            <span>{prod.prodName}</span>
          </div>
          {prod.colors.length > 0 && (
            <div className={classes.colors}>
              <span className="blind">색상 옵션</span>
              <ul>
                {prod.colors.map((val) => {
                  return (
                    <li key={val}>
                      <span
                        className={classes.colorChip}
                        style={{ backgroundColor: val }}
                      >
                        <span className="blind">{val}</span>
                      </span>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
          <div className={classes.priceBox}>
            <div className={classes.price}>
              <span className="blind">판매가격</span>
              <span className={classes.priceNum}>{prod.price}</span>
              <span className={classes.priceTxt}>원</span>
            </div>
            {prod.outOfStock && (
              <div className={classes.outOfStock}>
                <span className={classes.outOfStockTxt}>품절</span>
              </div>
            )}
          </div>
        </div>
      </Link>
    </div>
  );
};

export const MemoizedProductItem = React.memo(ProductItem);
